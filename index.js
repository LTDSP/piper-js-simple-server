const spawn = require('child_process').spawn;
const Transform = require('stream').Transform;
const { Serialise, Deserialise } = require('piper/JsonProtocol');
const { FeatureExtractorSynchronousService } = require('piper/FeatureExtractorService');
const { KissRealFft } = require('piper/fft/RealFft');
const ZeroCrossings = require('./zero-crossings');
const PowerSpectrum = require('./power-spectrum');
const split = require('split2');

const createExampleService = () => {
  const fftFactory = size => new KissRealFft(size);
  const plugins = [
    ZeroCrossings,
    PowerSpectrum
  ];
  return new FeatureExtractorSynchronousService(fftFactory, ...plugins);
};

class PiperStream extends Transform {
  constructor(service) {
    super({objectMode: true});
    this.service = service;
  }
  
  _transform(request, encoding, done){
    done(null, this.routeRequest(request) + "\n");
  }
  
  routeRequest(rpcRequest) {
    try {
      const id = rpcRequest.id;
      switch (rpcRequest.method) {
      case 'list':
        return Serialise.ListResponse(
          this.service.list(Deserialise.ListRequest(rpcRequest)),
          id
        );
      case 'load':
        return Serialise.LoadResponse(
          this.service.load(Deserialise.LoadRequest(rpcRequest)),
          id
        );
      case 'configure':
        return Serialise.ConfigurationResponse(
          this.service.configure(Deserialise.ConfigurationRequest(rpcRequest)),
          id
        );
      case 'process':
        return Serialise.ProcessResponse(
          this.service.process(Deserialise.ProcessRequest(rpcRequest)),
          true,
          id
        );
      case 'finish':
        return Serialise.FinishResponse(
          this.service.finish(Deserialise.FinishRequest(rpcRequest)),
          true,
          id
        );
      default: 
        throw new Error('Invalid request type.');
      }
    } catch (err) {
      // TODO hmm
      return JSON.stringify({error: {code: -32700, message: err.message}, id: null, method: rpcRequest.method});
    }
  }
}

function listenForStdin(piperConvertPath, service) {
  const requestConverter = spawn(
    piperConvertPath, 
    ['request', '-i', 'capnp', '-o', 'json-b64']
  );

  const responseConverter = spawn(
    piperConvertPath,
    ['response', '-i', 'json', '-o', 'capnp']
  );
  
  process.stdin.pipe(requestConverter.stdin);
  requestConverter.stdout
    .pipe(split(JSON.parse))
    .pipe(new PiperStream(service))
    .pipe(responseConverter.stdin);
  responseConverter.stdout.pipe(process.stdout);

  requestConverter.stderr.pipe(process.stderr);
  responseConverter.stderr.pipe(process.stderr);
  process.stdin.resume();
}

module.exports = {
  listenForStdin: listenForStdin,
  PiperStream: PiperStream,
  createExampleService: createExampleService
};