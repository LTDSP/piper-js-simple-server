const ZeroCrossings = require("piper/extractors/ZeroCrossings").default;
const InputDomain  = require('piper/FeatureExtractor').InputDomain;

const ZeroCrossingsMetadata = { 
  basic: { 
    description: 'Detect and count zero crossing points', 
    identifier: 'zc-es6', 
    name: 'Zero Crossings ES6' 
  }, 
  basicOutputInfo: [
    {
      description: 'The number of zero crossing points per processing block', 
      identifier: 'counts', 
      name: 'Zero Crossing Counts' 
    }, 
    { 
      description: 'The locations of zero crossing points', 
      identifier: 'crossings', 
      name: 'Zero Crossings' 
    } 
  ], 
  category: [], 
  copyright: 'Freely redistributable (BSD license)', 
  inputDomain: InputDomain.TimeDomain, 
  maker: 'JavaScript Crap', 
  maxChannelCount: 1, 
  minChannelCount: 1, 
  parameters: [],
  key: 'example-module:zc', 
  version: 1, 
  programs: [] 
};

module.exports = {create: sr => new ZeroCrossings(sr), metadata: ZeroCrossingsMetadata};