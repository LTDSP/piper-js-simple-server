#!/usr/bin/env node
const { listenForStdin, createExampleService  } = require('./index');

if (process.argv.length != 3) { 
  console.error("Path to piper-convert must be provided.");
  process.exit(1);
}

listenForStdin(process.argv[2], createExampleService());
