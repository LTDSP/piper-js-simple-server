const {SampleType, InputDomain} = require('piper/FeatureExtractor');

class PowerSpectrum {
  constructor() {
      this.binCount = 0;
  }

  configure(configuration) {
    this.binCount = 1 + 0.5 * configuration.framing.blockSize;
    this.magnitude = new Float32Array(this.binCount);
    const descriptor = {
      binCount: this.binCount,
      sampleType: SampleType.OneSamplePerStep,
      hasDuration: false
    };
    return {
      outputs: new Map([
        ["spectrum", descriptor]
      ]),
      framing: configuration.framing
    }
  }

  getDefaultConfiguration() {
    return {
      channelCount: 1, 
      framing: {
        blockSize: 0, 
        stepSize: 0
      }
    };
  }

  process(block) {
    const complex = block.inputBuffers[0]; // complex data is always in the first buffer?

    for (let i = 0; i < this.binCount; ++i) {
      const real = complex[i * 2];
      const imaginary = complex[i * 2 + 1];
      this.magnitude[i] = real * real + imaginary * imaginary;
    }
    return new Map([
      ["spectrum", [{featureValues: this.magnitude}]]
    ]);
  }

  finish() {
    return new Map();
  }
}

const PowerSpectrumMetaData = {
  basic: {
    description: "Perhaps simplest example of a frequency domain plugin.",
    identifier: "js-freq",
    name: "Power Spectrum"
  },
  basicOutputInfo: [
    {
      description: "The power spectrum",
      identifier: "spectrum",
      name: "Power Spectrum"
    }
  ],
  inputDomain: InputDomain.FrequencyDomain,
  maxChannelCount: 1,
  minChannelCount: 1,
  maker: 'JavaScript Crap', 
  key: "stub-freq:spectrum",
  version: 1,
  category: [], 
  copyright: 'Freely redistributable (BSD license)'
};

module.exports = {create: sr => new PowerSpectrum(sr), metadata: PowerSpectrumMetaData};
